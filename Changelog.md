## 0.2.0 (release date: 2018-12-13)
* Upgrade to clang 7. and head of google benchmark.
* Switch base to fedora:29
## 0.1.0 (release date: 2018-03-19)
* Upgrade the distribution, and move to newer release
  of clang-6
## 0.0.1 (release date: 2018-02-13)

* Initial release

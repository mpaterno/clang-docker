die()
{
  local exitval
  if [[ "$1" =~ ^[0-9]*$ ]]; then (( exitval = $1 )); shift; else (( exitval = 1 )); fi
  echo "ERROR: $@" 1>&2
  exit $exitval
}

STD_ARGS="--rm -it --volume $PWD/test:/test --workdir /test"
IMAGE="${NAME}:${VERSION}"

docker run ${STD_ARGS} ${IMAGE} bash -cl "rm -f bmark; clang++-7 -stdlib=libc++ -O3 -o bmark bmark.cc -lbenchmark -lpthread -lc++ ; ./bmark" || die "benchmark build and run failed"
echo "benchmark test OK"

FROM fedora:29
RUN dnf -y update \
  && dnf -y upgrade
RUN dnf -y update \
  && dnf -y install git \
  && dnf -y install cmake \
  && dnf -y groupinstall "Development Tools"
RUN dnf -y install clang-7.0.0 \
  && dnf -y install libcxx-devel
RUN mkdir -p /tmp/build \
  && cd /tmp/build \
  && git clone https://github.com/google/benchmark.git \
  && cd benchmark \
  && mkdir build \
  && cd build \
  && cmake .. -DCMAKE_CXX_COMPILER=/usr/bin/clang++-7 -DCMAKE_CXX_FLAGS="-stdlib=libc++" -DCMAKE_BUILD_TYPE=RELEASE -DBENCHMARK_DOWNLOAD_DEPENDENCIES=ON \
  && make -j4 \
  && make install \
  && cd / \
  && rm -rf /tmp/build

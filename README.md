## A Docker image for using Google benchmark with clang

This repository contains a Docker file (and GNUmakefile for building the Docker
image from it). This image contains the Google benchmark library, built with
clang 6, as well as the clang-6 compiler.
